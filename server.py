from aiohttp import web
from random import randrange
import json

import socketio

sio = socketio.AsyncServer(async_mode='aiohttp')
app = web.Application()
sio.attach(app)


@sio.event
async def command(sid, message):
    print("Received command with message: " + message)


async def send_welcome(message):
    print("Sneding welcome message: " + message)
    await sio.sleep(1)
    await sio.emit('hello', message)


async def send_sensor_data():
    for i in range(2):
        await sio.sleep(2)
        data = {'temperature': 30.0 + randrange(5), 'humidity': 75 + randrange(-10, 10)}
        print("Sending sensor data {}: {}".format(i, data))
        await sio.emit('sensor', json.dumps(data))


@sio.event
async def connect(sid, environ):
    print("Client connected")
    task1 = sio.start_background_task(send_welcome, "Hello!")
    task2 = sio.start_background_task(send_sensor_data)
    # await task1
    # await task2


@sio.event
def disconnect(sid):
    print('Client disconnected')


if __name__ == '__main__':
    web.run_app(app)
